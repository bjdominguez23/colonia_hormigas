import os
import numpy as np
import random
import pandas as pd
import time
from random import uniform
from collections import OrderedDict

def actualizar_matriz_probabilidades(hongo):
    suma = 0
    for pos in range(0,num_hongos):
        if distancia[hongo][pos]> 0:
            visibilidad = 1/distancia[hongo][pos]
        else:
            visibilidad = 0

        suma += ((visibilidad**beta) * (feromonas[hongo][pos]**alfa))
    for pos in range(0,num_hongos):
        if distancia[hongo][pos]> 0:
            visibilidad = 1/distancia[hongo][pos]
            candidatos[hongo][pos] = ((visibilidad**beta) * (feromonas[hongo][pos]**alfa)) / suma
            candidatos[pos][hongo] = candidatos[hongo][pos]
        else:
            candidatos[hongo][pos] = 0
            candidatos[pos][hongo] = 0
    return

def elegir_nuevo(hongo):
    cads = list()
    suma = 0
    for h,num in hongos.items():
         if (h,num) not in anterior and num not in aislados and distancia[hongo][num] > 0:
            cads.append((candidatos[hongo][num],num))
            suma += candidatos[hongo][num]
    if suma == 0:
        return ((0,0))
    aleatorio = uniform(0,suma)
    suma = 0
    pos = 0
    while suma <= aleatorio and pos < len(cads):
        suma += cads[pos][0]
        pos += 1

    for hongo,num in hongos.items():
        if num == cads[pos - 1][1]:
            llave = hongo
    return (llave,cads[pos - 1][1])

def actualizar_feromonas():
    for i in range(num_hongos):
        for j in range(i,num_hongos):
            feromonas[i][j] = ((1-p)*feromonas[i][j]) + depositada[i][j]
            feromonas[j][i] = ((1-p)*feromonas[j][i]) + depositada[j][i]
    return

def reducir_espacio_soluciones():
    for i in range(num_hongos):
        band = 0;
        for j in range(i,num_hongos):
            if aparecen[i][j] == 0:
                distancia[i][j] = 0
                distancia[j][i] = 0
        if list(distancia[i]).count(0) == num_hongos:
            if i not in aislados:
                aislados.append(i)
    return len(aislados)

#Obtener el directorio actual
act_dir = os.path.dirname(os.path.abspath(__file__))

#variable globales
hongos = {} #contiene los nombres de los hongos
hits = list() #lista de diccionarios cada uno contiene los cliques
sys_random = random.SystemRandom() #para generar los random
aislados = list() #guardar los nodos que han quedado aislados para poder llegar
comprobados = {}

#parámetros
alfa = 1 #peso de la feromona
beta = 0 #peso de la ruta
p = 0.01 #tasa de evaporacion de la feromona
hormigas = 50 #número de hormigas
Q = 1 #intensidad de la feromona depositada
iteraciones = 100
num_hongos = 33

#Leer Matriz de distancias
distancia = data = pd.read_excel("matriz_disimilaridad.xlsx",sheet_name="Disimilaridad",header=None)

#crear dictionario con los cliques de tamaño n
num = 0;
print('Procesando archivos de hits')
for i in os.listdir(act_dir):
    if i.endswith('.txt'):
        hongos[i[:-8]] = num #agregar nombre del hongo a lista
        num  = num + 1
        dic_aux = {} #crear un diccionario para guardas los diccionarios que contendran los cliques
        with open(i) as archivo:
           nombre = ""
           for linea in archivo:
               palabras = linea.split()
               if linea.startswith('Query'): #si comienza con query creamos un nuevo diccionario
                   if nombre is not "":
                       dic_aux[nombre] = dic
                   dic = {}
                   nombre = palabras[1]
               else:
                   dic[palabras[0]] = palabras[1]
        dic_aux[nombre] = dic
        hits.append(dic_aux)
print('Archivos de hits procesados')

for exp in range(1,4):
    tiempo = np.zeros(num_hongos + 1)
    inicial = sys_random.choice(list(hongos.items())) #seleccionar hongo inicial
    for tam in range(3,num_hongos + 1):
        inicio = time.time()
        print('Buscando cliques de tamaño ' + str(tam))
        #Declaración e inicialización de variables
        tam_clique = tam #Tamaño del clique a buscar
        Dhongos = np.zeros((num_hongos,num_hongos)) #Matriz de distancias
        feromonas = np.full((num_hongos,num_hongos),0.1) #Matriz de feromonas
        candidatos = np.zeros((num_hongos,num_hongos)) #Matriz de candidatos y para guardas las probabilidades
        aparecen = np.zeros((num_hongos,num_hongos))#guardar los hongos que han aparecido en los cliques
        
        #Busqueda
        #print('Inicio de la búsqueda')
        encontrados_anterior = 0
        no_nuevos = 0
        comprobados[tam_clique] = list()
        for iteracion in range(0,iteraciones):
            #print('Iteracion '+str(iteracion))
            cliques = list()
            depositada = np.zeros((num_hongos,num_hongos)) #Guardar la feromona que es depositada
            #inicial = sys_random.choice(list(hongos.items())) #seleccionar hongo inicial

            for hormiga in range(0,hormigas):
                intentos = 10 #Número de intentos de la hormiga
                pos = 0
                actual = inicial
                clique_acum = 1 
                proteinas = list()
                anterior = list() #para guardar los que ya forman parte del clique
                anterior.append(actual) #guardar el hongo actual
                while clique_acum < tam_clique and intentos > 0: 
                    #print(anterior)
                    actualizar_matriz_probabilidades(actual[1]) #actualizar la matriz de probabilidades
                    nuevo = elegir_nuevo(actual[1]) #elegir el siguiente hongo segun las probabilidades y los que ya forman parte del clique
                    if nuevo == (0,0):
                        if len(proteinas) > 0:
                            proteinas.pop()
                        break

                    #Si no tienen ninguna proteina en comun
                    if len(list(hits[actual[1]][nuevo[0]])) == 0:
                        break
                    
                    #Si es el primer elemento elegimos la proteina 
                    if len(proteinas) == 0:
                        proteina = sys_random.choice(list(hits[actual[1]][nuevo[0]])) #seleccionar una al azar
                        proteinas.append((actual,proteina)) #guardarla como tupla 
                        
                    #Comprobamos si existe un hit bidireccional
                    if proteinas[-1][1] in hits[actual[1]][nuevo[0]]: #[1] por que es una tupla (checar si es necesario guardar toda la tupla o solo la proteina)
                        proteinas.append((nuevo,hits[actual[1]][nuevo[0]][proteinas[-1][1]])) #si hace hit guardamos la proteina con que hace hit
                        
                        depositada[actual[1]][nuevo[1]] += Q/distancia[actual[1]][nuevo[1]] #Feromona depositada
                        depositada[nuevo[1]][actual[1]] += Q/distancia[nuevo[1]][actual[1]]
                        
                        aparecen[actual[1]][nuevo[1]] = 1 #para saber que aristar si aparecen en los cliques encontrados y quitar las que no
                        aparecen[nuevo[1]][actual[1]] = 1
                        
                        anterior.append(nuevo) #guardar el nuevo hongo
                        actual = nuevo  #El nuevo hongo se convierte en el actual
                        clique_acum += 1 #se incrementa en uno en contador para saber cuando terminar

                        #Checar si se ha formado un clique
                        if clique_acum == tam_clique: 
                            proteina_inicial = hits[proteinas[-1][0][1]][proteinas[0][0][0]].get(proteinas[-1][1])
                            if proteina_inicial == proteinas[0][1]: #si se ha formado lo guardamos
                                if proteinas not in cliques and proteinas not in comprobados[tam_clique]:
                                    cliques.append(proteinas)
                            else: #si no regresamos al anterior
                                intentos -=1
                                depositada[proteinas[-2][0][1]][proteinas[-1][0][1]] -= Q/distancia[proteinas[-2][0][1]][proteinas[-1][0][1]] #quitar la feromona que se deposito
                                proteinas.pop()
                                clique_acum -= 1
                                actual = proteinas[-1][0]
                    else:
                        intentos -= 1 #Si no hace hit entonces pierde un intento y no se guarda
                        #break;
            #Eliminar repetidos
            #cliques = [p for n, p in enumerate(cliques) if p not in cliques[:n]]
            #Verificar que sean cliques
            if tam_clique > 3: #si es mayor a tamaño 3 verificamos las aristas internas
                for clique in cliques:
                    band = 0;
                    for i in range(tam_clique - 2):
                        for j in range(i+2,tam_clique):
                            prot_1 = hits[clique[j][0][1]][clique[i][0][0]].get(clique[j][1])
                            if prot_1 != clique[i][1]: #Cuando no coinciden todos
                                band = 1
                    if band == 0: #Si coinciden todos lo guardamos
                        comprobados[tam_clique].append(clique)
            else:
                for clique in cliques:
                    comprobados[tam_clique].append(clique)
                
            encontrados = len(comprobados[tam_clique])
            #print(encontrados - encontrados_anterior)
            #print(len(cliques))
            if (encontrados - encontrados_anterior) == 0:
                no_nuevos = no_nuevos + 1
            elif no_nuevos > 0:
                no_nuevos = 0

            if no_nuevos == 10:
                iteracion = iteraciones
                break;

            encontrados_anterior = encontrados
            
            #actualizar matriz de feromonas
            actualizar_feromonas()
         
        #Quitar proteinas que no forman parte de ningun clique
        num_aislados = 0
        if tam_clique > 10:
            num_aislados = reducir_espacio_soluciones()
            #hormigas = 30

        tiempo[tam_clique] = time.time()-inicio #Tiempo final
        
        if tam_clique > (num_hongos - num_aislados):
            break
    #Guardar cliques encontrados
    unicos = {}
    if num_aislados == 0:
        max_encontrado = 33
    else:
        max_encontrado = num_hongos - num_aislados + 1
        
    for tam in range(3,max_encontrado + 1):
        unicos[tam] = list()
        for clique in comprobados[tam]:
            cadena = ""
            for c in clique:
                cadena = cadena + c[0][0] + ' ' + c[1] + '\n'
            unicos[tam].append(cadena)

    #Crear carpeta
    try:
        os.stat(str(exp))
    except:
        os.mkdir(str(exp))
        
    for i in range(3,max_encontrado + 1):
        file = open(str(exp)+ '/' + str(i)+'_limpio.clq','w')
        file.write("Tiempo: "+str(tiempo[i]))
        file.write("Cliques: "+str(len(unicos[i])))
        for c in unicos[i]:
            band = 0
            j = i + 1
            while j < (num_hongos - num_aislados) and band == 0:
                for c_2 in unicos[j]:
                    if c in c_2:
                        band = 1
                        break
                j = j + 1
            if band == 0:
                file.write('Clique\n')
                file.write(c + '\n')
        file.close()

input("Presiona una tecla para continuar...")
    

